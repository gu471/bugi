
#!/bin/bash

cd /media/home/lehrer

for DIR in *
do
        if [ -d "${DIR}" ]; then
                echo "lehrer-quota für: " + $DIR
                quotatool -u $DIR -b -q 1500M -l 1600M /media/home
                #chown $DIR:lehrer -R $DIR
        fi
done

#cd /media/home/schueler/Bugi
#
#for class in *
#do
#    for pupil in /media/home/schueler/Bugi/$class/*
#    do
#        if [ -d "${pupil}" ]; then
#               NAME=$(basename $pupil)
#                echo "schüler-quota für: " $NAME
#                quotatool -u $NAME -b -q 1000M -l 1100M /media/home
#                #chown $DIR:schueler -R $DIR
#        fi
#    done
#done

cd /media/home/schueler/

for DIR in *
do
        if [ -d "${DIR}" ]; then
                echo "schüler-quota für: " + $DIR
                quotatool -u $DIR -b -q 1000M -l 1100M /media/home
                #chown $DIR:lehrer -R $DIR
        fi
done
