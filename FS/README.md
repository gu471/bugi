# SETUP-log, inkonsistent(todo)

## S02 todo

	xrdp

## Installations Log, sofern nicht Standard

1. Hostname:

    `bugi01fs02.private.bugi.de`
	
2. Keine Verschlüsselung der Home-Ordner

3. Timezone Berlin

4. Mount-Points:
	+ für alle mount options: * user_xattr (für NTFS Rechte)
	- vda - 2TB: /media/home (Homelaufwerke aller Nutzer)
	- vbd - 400GB: /media/share_lehrer (Austauschlaufwerk für Lehrer (L:, V:))
	- vdc - 300GB: /media/share_schueler (Austauschlaufwerk für Schüler (S:, P:))

## Konfiguration
	
`sudo apt update && sudo apt upgrade`

RootLogin über ssh erlauben

	sudo su
	passwd 
	    #(danach root-pw eingeben)
	nano /etc/ssh/sshd_config
		#ändern: PermitRootLogin yes
	service sshd restart

Netzwerk festlegen (feste IP)

	nano /etc/network/interfaces
==> [/etc/network/interfaces](https://git.gu471.de/gu471/bugi/blob/master/FS/etc/network/interfaces)
		
DC02 in hosts fest eintragen:

`nano /etc/hosts`

	127.0.0.1 bugi01fs02.private.bugi.de bugi01fs02 localhost
    192.168.20.19   bugi01dc01.private.bugi.de bugi01dc01

Netzwerkauflösung testen mit:
		ping google.de # o.ä.
		host -t A BUGI01DC01.private.bugi.de
				
DNS-A-Record für 192.168.20.32 anlegen
		
NTP setzen:

    apt install ntp
	nano /etc/ntp.conf 
	
alle pool-Einträge mit "#" auskommentieren
TFK als NTP-Server hinzufügen:

    server 192.168.20.3

üperprüfen mit

	ntpq -p
			 remote           refid      st t when poll reach   delay   offset  jitter
		==============================================================================
		*192.168.20.3    LOCAL(0)        11 u    6   64    1    0.753   -0.186   0.000

## Samba installieren und in die Domain als Member einbinden:
(`net cache flush` nutzen um cache zu löschen bei Einstellungsänderungen)

	apt install winbind  samba  smbclient libnss-winbind libpam-winbind 
		#realm: private.bugi.de

Verbindung zum DC prüfen:

	getent hosts bugi01dc02
		192.168.20.20   bugi01dc02

Samba konfigurieren

`nano /etc/samba/smb.conf`

==> [/etc/samba/smb.conf](https://git.gu471.de/gu471/bugi/blob/master/FS/etc/samba/smb.conf)

Nutzer-Auth zur Domäne 

`nano /etc/nsswitch.conf`

    passwd:	... files winbind
    group:	... files winbind

==>[/etc/nsswitch.conf komplett](https://git.gu471.de/gu471/bugi/blob/master/FS/etc/nsswitch.conf)

Windbind verlinken

	ln -s /usr/local/samba/lib/libnss_winbind.so.2 /lib/x86_64-linux-gnu/
	ln -s /lib/x86_64-linux-gnu/libnss_winbind.so.2 /lib/x86_64-linux-gnu/libnss_winbind.so
	ldconfig
	
ACL-Support einschalten:

	nano /etc/fstab
		#vor user_xattr einfügen:
			defaults,barrier=1,

Server neustarten

	reboot

Server in die Domäne buchen
			
	net ads join -U administrator
	
### Samba testen und Freigaben einrichten

ACL Support testen:

	smbd -b | grep HAVE_LIBACL

Share für Schüler einrichten (Gruppen Laufwerk)
{nicht aktuelle Beispielkonfiguration}

	cd /media/share_schueler
	mkdir -p bugi/schueler_lw
	chmod g=rwx -R bugi
	chgrp "domänen-admins" -R bugi
	chown "administrator" -R bugi
		
	[Bugi_Schueler_LW$]
       path = /media/share_schueler/bugi/schueler_lw/
       read only = no

	service smbd restart	
	
### QUOTA

winbind Verbindung testen

		wbinfo -p
			Ping to winbindd succeeded
			
werden Nutzer von der Domäne erkannt?

		wbinfo -u
			(Nutzerliste der AD)
			
werden AD-Nutzer lokal aufgelöst?

		getent passwd
			(Liste mit IDs)
			
werden Gruppenbeziehungen sauber aufgelöst?

		id administrator
			(zeigt ids vom admin an sowie alle Gruppenzugehörigkeiten inkl. ids)

Partition mit Quota-Optionen einbinden

	nano /etc/fstab
		UUID=1cf9c2e7-deea-4d54-b763-66898ab950ec /media/home     ext4    defaults,barrier=1,user_xattr      0       2
		#zu:
		UUID=1cf9c2e7-deea-4d54-b763-66898ab950ec /media/home     ext4    defaults,barrier=1,user_xattr,usrquota,grpquota      0       2

Partition neu einbinden

	umount /media/home
	mount -a	

Quota-Check für alle Nutzer, für alle Partitionen
(sollte nur durchgeführt werden, wenn keine Nutzer aktiv sind. Ist nur notwendig, wenn Dateien nicht über die Freigabe, sondern unter Linux in Ordner kopiert werden)

	quotacheck -avu
		
Quota für Nutzer einstellen

	edquota tst.user

Quota-Werte und -Nutzung zeigen

	repquota /media/home

Quota anschalten
(quotacheck wird im laufenden Betrieb ausgeführt, manuelles ausführen durch `quotacheck` nur nach deaktivieren von quota: `quotaoff -a`)
		
	quotaon -a

Quotatool installieren (für automatisiertes Setzen der Quota)

		apt install quotatool

Automatische setzen der quota über das Script [/media/home/setquota.sh](https://git.gu471.de/gu471/bugi/blob/master/FS/media/home/setquota.sh)
Ausführen über [/etc/crontab](https://git.gu471.de/gu471/bugi/blob/master/FS/etc/crontab)