param([switch]$Elevated)

function Test-Admin {
      $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())

$currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

if ((Test-Admin) -eq $false)  {
        if ($elevated)
        {
            # tried to elevate, did not work, aborting
        }
        else {
            Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
}

exit
}

if ( (Get-WmiObject -Class win32_computersystem).Domain -like "*bugi.de")
{
        Write-Host "Already in Domain, exit."
        Exit
}
else
{
        Write-Host "not in domain, proceeding"
}

$fogscript = "http://192.168.23.1/fog/getdata.php"
$hostname = hostname

$date = Get-Date -UFormat "%Y%m%d_%A_%H%M%S"
$logfile = ("log_adjoin_"+$date+".txt")


if (!(Test-Path $logfile))
{
        New-Item -name $logfile -type "file" -Value "AD join log `r`n"
}

Function log
{
        param([string]$ls)
        Add-Content $logfile -value $ls
}

$wc = New-Object System.Net.WebClient
$url = $fogscript + "?hostname=" + $hostname + "&action=hosts"
log ("get hosts-file: " + $url + "`r`n")
$wc.DownloadFile($url, "$PSScriptRoot\hosts")
$url = $fogscript + "?hostname=" + $hostname + "&action=ou"
log ("get ou-file: " + $url + "`r`n")
$wc.DownloadFile($url, "$PSScriptRoot\ou")
$url = $fogscript + "?hostname=" + $hostname + "&action=server"
log ("get server-file: " + $url + "`r`n")
$wc.DownloadFile($url, "$PSScriptRoot\server")

if ( (Get-Content "$PSScriptRoot\hosts") -like "192.168.*private.bugi.de")
{
        log "hosts ok: `r`n"
        log ((Get-Content "$PSScriptRoot\hosts") + "`r`n")
        Copy-Item $PSScriptRoot\hosts C:\Windows\system32\drivers\etc\hosts
}
else
{
        log "bad hosts: `r`n"
        log ((Get-Content "$PSScriptRoot\hosts") + "`r`n")
}

if (((Get-Content "$PSScriptRoot\ou").Replace(" ","") -like "OU*DC=private,DC=bugi,DC=de") -And ((Get-Content "$PSScriptRoot\server").Replace(" ","") -like "BUGI*DC*"))
{
        log "ou ok: `r`n"
        log ((Get-Content "$PSScriptRoot\ou") + "`r`n")
        log "server ok: `r`n"
        log ((Get-Content "$PSScriptRoot\server") + "`r`n")
        $username = "BUGI\login.domain"
        $password = "SETPASSWORDHERE" | ConvertTo-SecureString -asPlainText -Force
        $credential = New-Object System.Management.Automation.PSCredential($username,$password)
        log (Add-Computer -DomainName "private.bugi.de" -Server (Get-Content "$PSScriptRoot\server") -Verbose -Force -PassThru -Credential $credential -OUPath (Get-Content "$PSScriptRoot\ou") )
        $RegKey ="HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
        Set-ItemProperty -Path $RegKey -Name ForceAutoLogon -Value 0
        Set-ItemProperty -Path $RegKey -Name AutoAdminLogon -Value 0
        Stop-Computer

}
else
{
        log "bad ou or server: `r`n"
        log ((Get-Content "$PSScriptRoot\ou") + "`r`n")
        log ((Get-Content "$PSScriptRoot\server") + "`r`n")
}