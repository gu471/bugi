setLocal EnableDelayedExpansion

set variable1=%ComputerName%
set variable3=FogImage

if not "x!variable1:%variable3%=!"=="x%variable1%" (goto start)

endlocal

if exist "C:\cmds\ready" (goto end)
if exist "C:\cmds\init" (goto ad)
goto end

:start
mkdir "C:\cmds\init"
cd "C:\cmds"
PowerShell.exe -ExecutionPolicy Bypass -File "changehostname.ps1"
pause


:ad
mkdir "C:\cmds\ready"

if exist "C:\cmds\sysprep" (rmdir /Q /S "C:\cmds\sysprep")

cd "C:\cmds"
PowerShell.exe -ExecutionPolicy Bypass -File "adjoin.ps1"
pause

:end