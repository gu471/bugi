param([switch]$Elevated)

function Test-Admin {
  $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
  $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

if ((Test-Admin) -eq $false)  {
    if ($elevated)
    {
        # tried to elevate, did not work, aborting
    }
    else {
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
}

exit
}

$fogscript = "http://192.168.23.1/fog/getdata.php"

$date = Get-Date -UFormat "%Y%m%d_%A_%H%M%S"
$logfile = ("log_hostname_"+$date+".txt")


if (!(Test-Path $logfile))
{
    New-Item -name $logfile -type "file" -Value "hostname log `r`n"
}

Function log
{
    param([string]$ls)
    Add-Content $logfile -value $ls
}

$colItems = get-wmiobject -class "Win32_NetworkAdapterConfiguration" -computername 'localhost' |Where{$_.IpEnabled -Match "True"} 
    
foreach ($objItem in $colItems) { 
   
    $MAC = ($objItem |select MACAddress).MACAddress

    $wc = New-Object System.Net.WebClient
    $url = $fogscript + "?MAC=" + $MAC + "&action=getMAC"
    log ("get hostname for MAC(" + $MAC + ") : " + $url + "`r`n")
    $wc.DownloadFile($url, "$PSScriptRoot\MAC")

    if ( (Get-Content "$PSScriptRoot\MAC") -ne "" )
    {
        $hostname = (Get-Content "$PSScriptRoot\MAC")
        log ("found hostname for MAC(" + $MAC + ") : " + $hostname + "`r`n")
    }
}

if ($hostname -eq "")
{
    log ("not hostname found, canceling `r`n")
}
else
{
    log ("found hostname for MAC(" + $MAC + ") : " + $hostname + "`r`n")
    Rename-Computer -NewName $hostname -Force -PassThru -Restart
    #Restart-Computer
}