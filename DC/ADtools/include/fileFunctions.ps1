#csv choose dialog
function chooseCSVfile
{
	param([string]$Title,[string]$Directory,[string]$Filter="CSV Files (*.csv)|*.csv")
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$openFileDialog = New-Object System.Windows.Forms.OpenFileDialog
	$openFileDialog.InitialDirectory = $Directory
	$openFileDialog.Filter = $Filter
	$openFileDialog.Title = $Title
	$openFileDialog.ShowHelp = $true
	
	$Show = $openFileDialog.ShowDialog()
	
	If ($Show -eq "OK")
	{
		Return $openFileDialog.FileName
	}
	Else
	{
		Exit
	}
}

#clear first line and sample-user
function cleanCSV
{
	param([string]$FileName)
	$newFileName = $FileName.replace(".csv", ".ccsv")
	
	##Copy-Item -Path $FileName -Destination $newFileName
	get-content $FileName |
		select -Skip 1 |
		Where-Object {$_ -notmatch 'man.musterschueler'} |
		set-content $newFileName
	#move "$file-temp" $newFileName -Force
	
	Return $newFileName
}

#log Passwords for user
function savePassword
{
	param(
		[string]$Name,
		[string]$Vorname,
		[string]$SAM,
		[string]$Passwort
		)
		
	if(![System.IO.File]::Exists($Global:export))
	{
		"Name,Vorname,SAM,Passwort" >> $Global:export
	}
	
	$Name + "," + $Vorname + "," + $SAM + "," + $Passwort >> $Global:export
}