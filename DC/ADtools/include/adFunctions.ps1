Function createGroup
{
	param(
		[string]$Group,
		[string]$Path
	)
	$FullPath = $Global:OUGruppen + $Path
	Write-Host " Gruppe hinzufügen? | CN="$Group"," $FullPath -ForegroundColor Yellow
	Write-Host " [J]a, [N]ein | [N] : " -ForegroundColor Yellow -NoNewline
	$result = Read-Host
	if ($result -eq "J" -or $result -eq "j")
	{
		$Group_Lehrer = "Lehrer_" + $Group
		New-ADGroup -Name $Group -SamAccountName $Group -GroupCategory Security -GroupScope Global -DisplayName $Group -Path $FullPath
		New-ADGroup -Name $Group_Lehrer -SamAccountName $Group_Lehrer -GroupCategory Security -GroupScope Global -DisplayName $Group_Lehrer -Path ("OU=Lehrer_Gruppen," + $FullPath)
	}
}

Function createOU
{
 param(
	[string]$OU,
	[string]$Path
)
$FullPath = $Path
Write-Host " OU hinzufügen? | CN="$OU"," $FullPath -ForegroundColor Yellow
Write-Host " [J]a, [N]ein | [N] : " -ForegroundColor Yellow -NoNewline
$result = Read-Host
if ($result -eq "J" -or $result -eq "j")
{
	New-ADOrganizationalUnit -Name $OU -Path $Path
}
}

Function addToGroup
{
	param(
		[string]$UserPath,
		[string]$GroupPath
		)
		
	$user = Get-ADUser $UserPath
	$group = Get-ADGroup $GroupPath
	Add-ADGroupMember $group -Member $user
}

Function createUser
{
 param(
	[string]$OU_Basis,
	[string]$OU_Knoten,
	[string]$SAM,
	[string]$Name,
	[string]$Vorname,
	[string]$Passwort,
	[string] $Profil,
	[boolean]$Commit = $FALSE
)

$Error = 0

Write-Host $OU_Basis "  " $OU_Knoten

$Group = Get-ADGroup -SearchBase $OU_Basis -Filter {ObjectClass -eq 'group' -And Name -eq $OU_Knoten}

Write-Host " "$SAM "("$Name"," $Vorname ")" -ForegroundColor DarkGray
Write-Host "    Passwort:" $Passwort -ForegroundColor DarkGray

$OU_Name = $OU_Knoten + "," + $OU_Basis
$OU = Get-ADObject -Filter {ObjectClass -eq 'organizationalunit' -And Name -eq $OU_Knoten } -SearchBase $OU_Basis

if ($Commit -eq $FALSE)
{
	if ($Group -eq $NULL)
	{
		Write-Host "    Gruppe: es wurde keine Gruppe gefunden für" $OU -ForegroundColor Red
		$Error = $Error + 1
		createGroup -Group $OU_Knoten -Path $OU_Basis
	}
	elseif ($Group.GetType().FullName -eq "Microsoft.ActiveDirectory.Management.ADGroup")
	{
		Write-Host "    Gruppe:" $Group.distinguishedName
	}
	else
	{
		Write-Host "    Gruppe: es wurde mehr als eine mögliche Gruppe gefunden für" $OU_Knoten ":" -ForegroundColor Red
		foreach ($Gruppe in $Group)
		{
			Write-Host "       " $Gruppe.distinguishedName -ForegroundColor Red
		}
		$Error = $Error + 1
	}

	if ($OU -eq $NULL)
	{
		Write-Host "    OU: es wurde keine OU gefunden für" $OU_Knoten -ForegroundColor Red
		$Error = $Error + 1
		createOU -OU $OU_Knoten -Path $OU_Basis
	}
	elseif ($OU.GetType().FullName -eq "Microsoft.ActiveDirectory.Management.ADObject")
	{
		Write-Host "    OU:    " $OU
	}
	else
	{
		Write-Host "    OU: es wurde mehr als eine mögliche OU gefunden für" $OU_Knoten ":" -ForegroundColor Red
		foreach ($O in $OU)
		{
			Write-Host "       " $O.distinguishedName -ForegroundColor Red
		}
		$Error = $Error + 1
	} 

	Write-Host "    Profil:" $Profil
}
elseif ($Error -lt 1)
{
	$DisplayName = $Name + ", " + $Vorname
	$UPN = $SAM + "@" + $Global:Domain
	$FullPath = "CN=" + $SAM + "," + $OU
	
	$User = Get-ADUser -Filter {SamAccountName -eq $SAM}
	
	if ($User -eq $NULL)
	{
		New-ADUser -Name $SAM -AccountPassword (ConvertTo-SecureString -AsPlainText $Passwort -Force) -ChangePasswordAtLogon $true -DisplayName $DisplayName -Enabled $true -GivenName $Vorname -HomeDirectory $Profil -HomeDrive $Global:HomeDrive -Path $OU -SamAccountName $SAM -Surname $Name -UserPrincipalName $UPN
		addToGroup -GroupPath $Group -UserPath $FullPath
		Write-Host "    angelegt" -ForegroundColor Green
		savePassword -Name $Name -Vorname $Vorname -SAM $SAM -Passwort $Passwort
		Return 0
	}
	elseif (-not ($User -like $FullPath))
	{
		Write-Host "   " $SAM "bereits vorhanden unter:" -ForegroundColor Black -BackgroundColor Red
		Write-Host "   " $User -ForegroundColor Black -BackgroundColor Red
		Write-Host "   " "soll aber werden:" -ForegroundColor Black -BackgroundColor Red
		Write-Host "   " $FullPath -ForegroundColor Black -BackgroundColor Red
		savePassword -Name $Name -Vorname $Vorname -SAM $SAM -Passwort "(vorhanden)"
	}
	else
	{
		Write-Host "    vorhanden" -ForegroundColor Yellow
		savePassword -Name $Name -Vorname $Vorname -SAM $SAM -Passwort "(vorhanden)"
		Return 0
	}
}

Return [int]$Error
}

Function workOnAD
{
 param($UserInformation,[int]$Stufe,[boolean]$Commit = $FALSE)

$Error = 0

ForEach ($User in $UserInformation)
{
	$Name = $User.Name
	$Vorname = $User.Vorname
	$SAM = $User.Login
	$Passwort = Get-Random -Minimum 1000 -Maximum 9999
	$Passwort = "Wolke"  + $Passwort

	if ($Stufe -eq 34)
	{
		$OU = $User.Klasse
		$OU_B = $Global:OUKlassen
		$Profil = $Global:FS_34 + $SAM
	}
	elseif ($Stufe -eq 5)
	{
		$OU = $User.OU
		$OU_B = $Global:OUKurse
		$Profil = $Global:FS_5 + $SAM
	}
	else
	{
		Write-Output "Fehler, Stufeninformation (workOnAD in adFunctions.ps1 fehlerhaft)"
		Exit
	}

	$act = createUser -Commit $Commit -OU_Basis $OU_B -OU_Knoten $OU -SAM $SAM -Name $Name -Vorname $Vorname -Passwort $Passwort -Profil $Profil
	$Error = $Error + $act
}
if ($Error -gt 0)
{
	Write-Host $Error "mögliche Inkonsistenzen gefunden, AD wurde NICHT aktualisiert" -ForegroundColor Black -BackgroundColor Red
	Write-Host "Script erneut ausführen, um erneut zu prüfen" -ForegroundColor Black -BackgroundColor Red
	Exit
}
elseif ($Commit -eq $true)
{
	Write-Host "AD wurde mit den gegebenen Informationen aktualisiert"
}

}