$OU_UserBase = "OU=10 Alsterdorf Campus,DC=private,DC=bugi,DC=de"
$OU_Klassen = "OU=Klassen, OU=Bugi, OU=Schüler," + $OU_UserBase
$OU_Kurse ="OU=Kurse, OU=Bugi, OU=Schüler," + $OU_UserBase

$Gruppen_Klassen = Get-ADGroup -SearchBase ("OU=Gruppen," + $OU_Klassen) -Filter {ObjectClass -eq 'group'} -SearchScope 1
$Gruppen_Kurse = Get-ADGroup -SearchBase ("OU=Gruppen," + $OU_Kurse) -Filter {ObjectClass -eq 'group'} -SearchScope 1

Function createDirIfNotExists
{
 param(
	[string]$Path
 )
	If (!(Test-Path $Path)) { 
   		New-Item -Path $Path -ItemType Directory 
	}
}

Function createUserDirIfNotExists
{
 param(
	[string]$Path,
	[string]$Username
 )
	If (!(Test-Path $Path)) { 
   		New-Item -Path $Path -ItemType Directory 
		
		$Acl = (Get-Item $Path).GetAccessControl('Access')
		$Username = "BUGI\" + $Username.Replace("@private.bugi.de", "")
		Write-Host $Username
		
		$acl = Get-Acl $Path
		$permission = $Username,"FullControl","ContainerInherit,ObjectInherit","None","Allow"
		$accessrule = New-Object system.security.AccessControl.FileSystemAccessRule $permission
		$acl.SetAccessRule($accessrule)
		set-acl -aclobject $acl $Path
	}
}

foreach ($Klasse in $Gruppen_Klassen)
{
	$Path = "W:\Bugi\Klassen\" + $Klasse.Name
	createDirIfNotExists -Path $Path
	#$Path = "Y:\Bugi\" + $Klasse.Name
	#createDirIfNotExists -Path $Path
}

foreach ($Kurs in $Gruppen_Kurse)
{
	$Path = "W:\Bugi\Kurse\" + $Kurs.Name
	createDirIfNotExists -Path $Path	
	#$Path = "Y:\Bugi\" + $Kurs.Name
	#createDirIfNotExists -Path $Path
}

$Schüler = Get-ADUser -SearchBase ("OU=Schüler," + $OU_UserBase) -Filter {ObjectClass -eq 'User' -And enabled -eq $true} -Properties "HomeDirectory"
foreach ($S in $Schüler)
{
	createUserDirIfNotExists -Path $S.HomeDirectory -Username $S.UserPrincipalName
}

$Lehrer = Get-ADUser -SearchBase ("OU=Lehrer," + $OU_UserBase) -Filter {ObjectClass -eq 'User' -And enabled -eq $true} -Properties "HomeDirectory"
foreach ($L in $Lehrer)
{
	createUserDirIfNotExists -Path $L.HomeDirectory -Username $L.UserPrincipalName
}

#$ProfilOrder_Schüler = Get-ChildItem Y:Bugi -Directory

$KlassenOrdner = Get-ChildItem W:Bugi\Klassen -Directory
$KursOrdner = Get-ChildItem W:Bugi\Kurse -Directory

foreach ($Kurs in ($KursOrdner + $KlassenOrdner)) {
    $Path = $Kurs.FullName
    $Acl = (Get-Item $Path).GetAccessControl('Access')
    $Username = "BUGI\" + $Kurs.Name
	Write-Host $Username
	
	$acl = Get-Acl $Path
	$permission = $Username,"DeleteSubdirectoriesAndFiles, ReadAndExecute, Write","None","None","Allow"
	$accessrule = New-Object system.security.AccessControl.FileSystemAccessRule $permission
	$acl.SetAccessRule($accessrule)
	#set-acl -aclobject $acl $Path

	$acl = Get-Acl $Path
	$permission = $Username,"DeleteSubdirectoriesAndFiles, Modify, Synchronize","ContainerInherit,ObjectInherit","InheritOnly","Allow"
	$accessrule = New-Object system.security.AccessControl.FileSystemAccessRule $permission
	$acl.AddAccessRule($accessrule)
	set-acl -aclobject $acl $Path

	$Username = "Lehrer_" + $Kurs.Name
		
	$Group = Get-ADGroup -SearchBase $OU_UserBase -Filter {ObjectClass -eq 'group' -And Name -eq $Username}
	
	if ($Group -ne $NULL -And $Group.GetType().FullName -eq "Microsoft.ActiveDirectory.Management.ADGroup")
	{	
		$Username = "BUGI\" + $Username
		Write-Host $Username
		$acl = Get-Acl $Path
		$permission = $Username,"DeleteSubdirectoriesAndFiles, ReadAndExecute, Write","None","None","Allow"
		$accessrule = New-Object system.security.AccessControl.FileSystemAccessRule $permission
		$acl.SetAccessRule($accessrule)
		#set-acl -aclobject $acl $Path
	
		$acl = Get-Acl $Path
		$permission = $Username,"DeleteSubdirectoriesAndFiles, Modify, Synchronize","ContainerInherit,ObjectInherit","InheritOnly","Allow"
		$accessrule = New-Object system.security.AccessControl.FileSystemAccessRule $permission
		$acl.AddAccessRule($accessrule)
		set-acl -aclobject $acl $Path
	}	
}