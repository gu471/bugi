Das Exceltabellenblatt so exportieren, dass keine leeren grünen Felder übrig sind.

Dateinamen:
	Stufe III/IV:
		*.34.csv
	Stufe V:
		*.5.csv
		
Reine Nutzdaten werden automatisch unter der Endung *.ccsv gespeichert.

Importieren (Klassenlisten):
	Script ausführen in der Powershell mit
		cd (Ordner zur import.ps1)
		.\import.ps1

	Datei wählen, die importiert werden soll.
	Im ersten Durchlauf wird überprüft, ob alle Gruppen und OUs vorhanden sind.
	Auf Nachfrage werden diese in der entsprechend festgelegten OU erzeugt.
	Das Script bricht bei Fehlern ab. Ein weiteres Ausführen sollte ohne Fehler enden.
	Jetzt kann überprüft werden, ob alle Daten so eingetragen würde, wie es sein sollte.
	Stimmen alle Daten, kann man mit "j" oder "J" das Script veranlassen nocheinmal durchzulaufen,
	aber jetzt mit eintragen der Daten in die AD.
	Im gleichen Zug, werden die neu angelegten Nutzer in die entsprechende Klassengruppe eingetragen.
	Zudem wird im Ordner "Passwords" eine csv mit den Passwörtern angelegt.
	
	Wurde ein Nutzer angelegt oder ist dieser schon vorhanden wird dies entsprechend angezeigt.
	Gibt es Nutzer, die in einer falschen OU liegen, aber angelegt werden sollen, dann zeigt das Script
	den Nutzer mit alter und neuer OU in rot an. Dieser Nutzer muss dann entweder per Hand umgezogen werden,
	oder gelöscht werden und das Script muss nochmal durchlaufen. WICHTIG: in der neue Passwortdatei werden die
	Passwörter der anderen Nutzer dann mit "(vorhanden)" angegeben, da deren Passwörter nicht nocheinmal
	geändert werden.