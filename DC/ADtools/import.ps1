Import-Module ActiveDirectory

. "include\adFunctions.ps1"
. "include\fileFunctions.ps1"

#CONFIG ######################
#StandardOU zum suchen und hinzufügen
$Global:OUBasis = "OU=Bugi,OU=Schüler,OU=10 Alsterdorf Campus,DC=private,DC=bugi,DC=de"
#OU für III/IV
$Global:OUKlassen = "OU=Klassen" + "," + $Global:OUBasis
#OU für V und Kurse
$Global:OUKurse = "OU=Kurse" + "," + $Global:OUBasis
#Sub OU der beiden oberen für die Sicherheitsgruppen
$Global:OUGruppen = "OU=Gruppen" + ","

# \ am Ende nicht vergessen!
#Hauptpfad zum Share
$Global:FS_Pfad = "\\192.168.20.32\Profil_Schueler$\bugi\"
#Subshare für III/IV
$Global:FS_34 = $Global:FS_Pfad #+ "III_IV\"
#Subshare für V
$Global:FS_5 = $Global:FS_Pfad #+ "V\"

#Heimverzeichnis auf U:
$Global:HomeDrive = "U"
#Domain für UPN
$Global:Domain= "private.bugi.de"

#CONFIG END ####################

$Stufe = 0
$FileName = chooseCSVfile -Title "Import an CSV file" -Directory "C:\import\"

$date = Get-Date -UFormat "%Y%m%d_%A_%H%M%S"
$Global:export= "passwords\" + ([System.IO.Path]::GetFileNameWithoutExtension($FileName)) + "__" + $date + ".csv"
#$Global:export= $Global:export + "__" + ([System.IO.Path]::GetFileNameWithoutExtension($FileName)) + ".csv"

if ($FileName.contains(".34."))
{
	$Stufe = 34
	echo "Importiere für Stufe III/IV"
}
elseif ($FileName.contains(".5."))
{
	$Stufe = 5
	echo "Importiere für Oberstufe"
}
else
{
	echo "Bitte Stufe angeben über Dateinamen: "
	echo "  Stufe III/IV : import_***.34.csv"
	echo "  Oberstufe    : import_***.5.csv"
	
	Exit
}

$UserInformation = (cleanCSV -FileName $FileName) | Import-Csv -Delimiter ','

echo "Überprüfe nach möglichen strukturellen Inkonsistenzen"

workOnAD -UserInformation $UserInformation -Stufe $Stufe

Write-Host " Überprüfung erfolgreich, neue Nutzer erstellen?" -ForegroundColor Yellow -BackgroundColor DarkCyan
Write-Host " [J]a, [N]ein | [N] : " -ForegroundColor Yellow -NoNewline -BackgroundColor DarkCyan
$result = Read-Host
if ($result -eq "J" -or $result -eq "j")
{
	workOnAD -UserInformation $UserInformation -Stufe $Stufe -Commit $True
}