<?php
        $action = htmlspecialchars($_GET["action"]);
//      echo $hostname . "  " . $action;

if ($action == "ou" || $action == "hosts" || $action == "server")
{
        $hostname = htmlspecialchars($_GET["hostname"]);

        $pdo = new PDO('mysql:host=localhost;dbname=fog', 'root', '');

        $sql = "SELECT hostID FROM hosts WHERE hostName = (:hostname)";
        $ru = $pdo->prepare($sql);
        $ru->bindParam(":hostname",$_GET["hostname"],PDO::PARAM_STR);
        $ru->execute();
        $result = $ru->fetch();
        $hostID = $result["hostID"];
//      echo "HID " . $hostID;

        $sql = "SELECT gmGroupID FROM groupMembers WHERE gmHostID = " . $hostID ;
        $ru = $pdo->prepare($sql);
        $ru->execute();
        $result = $ru->fetch();
        $groupID = $result["gmGroupID"];
//      echo "GID " . $groupID;

        $sql = "SELECT groupDesc FROM groups WHERE groupID = " . $groupID ;
        $ru = $pdo->prepare($sql);
        $ru->execute();
        $result = $ru->fetch();
        $groupDesc = $result["groupDesc"];
//        echo "Desc " . $groupDesc;

/*
        foreach ($pdo->query($sql) as $row) {
                var_dump($row);
        }
*/
        if ($action=="ou")
        {
                preg_match( '/ou\{(.*?)\}/', $groupDesc, $reg );
                die($reg[1]);
        }
        elseif ($action=="hosts")
        {
                preg_match( '/hosts\{(.*?)\}/', $groupDesc, $reg );
                die($reg[1]);
        }
        elseif ($action=="server")
        {
                preg_match( '/server\{(.*?)\}/', $groupDesc, $reg );
                die($reg[1]);
        }
}

elseif ($action == "getMAC")
{
        $pdo = new PDO('mysql:host=localhost;dbname=fog', 'root', '');

        $sql = "SELECT hmHostID FROM hostMAC WHERE (hmPrimary = '1' && hmMAC = (:MAC));";
        $ru = $pdo->prepare($sql);
        $ru->bindParam(":MAC",$_GET["MAC"],PDO::PARAM_STR);
        $ru->execute();
        $result = $ru->fetch();
        $hostID = $result["hmHostID"];

//      echo $hostID;

        $sql = "SELECT hostName FROM hosts WHERE hostID = " . $hostID ;
        $ru = $pdo->prepare($sql);
        $ru->execute();
        $result = $ru->fetch();
        $hostName = $result["hostName"];

        echo $hostName;
}
?>