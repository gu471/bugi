#!/bin/sh

mkdir /ntfs &>/dev/null;
mount.ntfs-3g /dev/sda2 /ntfs;

ceol=`tput el`;
machine=`dmidecode -s system-product-name`;
echo "${machine}"

if [ $osid == “5” ]; then
osn=“Win7”
elif [ $osid == “6” ]; then
osn=“Win8”
elif [ $osid == “7” ]; then
osn=“Win8.1”
fi
echo “Downloading Drivers”;
mkdir /ntfs/Drivers &>/dev/null;
echo -n “In Progress. Please wait.”;
cp -r "/images/drivers/${machine}"/* /ntfs/Drivers; #&>/dev/null;

rm /ntfs/cmds/install.cmd
cp /images/drivers/install.bat /ntfs/cmds/install.bat
cp /images/drivers/adjoin.ps1 /ntfs/cmds/adjoin.ps1
cp /images/drivers/changehostname.ps1 /ntfs/cmds/changehostname.ps1


umount /ntfs
echo -e “Done”;
